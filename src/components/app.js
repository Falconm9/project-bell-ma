import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

//components
import Header from './layer/header';
import Footer from './layer/footer';
import Home from '../sections/home.js';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Header />
          <Route exact path="/" component={Home}/>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
