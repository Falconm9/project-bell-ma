import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
  return(
    <header>
      <div className="title-conteiner">
        <Link to="/" className="logo-header">
          <span>
            Bellotero.io
          </span>
        </Link>
        <nav>
          <Link to="/">
            Features
          </Link>
          <Link to="/">
            Solutions
          </Link>
          <Link to="/">
            Stories
          </Link>
          <Link to="/">
            Partners
          </Link>
          <Link to="/">
            About
          </Link>
          <Link to="/">
            Blog
          </Link>
        </nav>
        <nav>
          <Link to="/">
            Request a Demo
          </Link>
        </nav>
        <nav>
          <Link to ="/">
            Log In
          </Link>
        </nav>
      </div>
    </header>
  )
}

export default Header;
