import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => {
  return(
    <header>
      <div className="title-conteiner">
        <Link to="/" className="footer-copyright">
            © 1909 Bellotero.io
        </Link>
        <nav>
          <Link to="/">
            Privacy Policy
          </Link>
          <Link to ="/">
            Terms of Service
          </Link>
        </nav>
      </div>
    </header>
  )
}

export default Footer;
